'use strict'
const {src, dest, watch, parallel, series} = require('gulp'); // підключення плагіну gulp


// підключення плагінів
const scss = require('gulp-sass')(require('sass'));
const concat= require('gulp-concat');
const uglify = require('gulp-uglify-es').default;
const browserSync = require('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');
const clean = require('gulp-clean');
const avif = require('gulp-avif');
const webp = require('gulp-webp');
const imagemin = require('gulp-imagemin');
const newer = require('gulp-newer');
const fonter = require('gulp-fonter');
const ttf2woff2 = require('gulp-ttf2woff2');
const svgSprite = require('gulp-svg-sprite');
const include = require('gulp-include');

// function для конкретинації файлів  з scss до min.css
function styles() {
    return src('app/scss/style.scss')
        .pipe(scss({outputStyle: 'compressed'}))
        .pipe(concat('style.min.css'))
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 10 version'],
            grid: true
        }))
        .pipe(dest('app/css'))
        .pipe(browserSync.stream());
}
// function for compress images
function images() {
    return src(['app/images/src/*.*', '!app/images/src/*.svg']) //конвертуємо всі файли окрім SVG
        .pipe(newer('app/images')) //кешуємо сконвертовані фото. Необхідно прописати кожному формату нижче..
        .pipe(avif({ quality: 50 }))

        .pipe(src('app/images/src/*.*')) //для WEBP не потрібно обмежувати конвертування svg, він сам розуміє що його не потрібно конвертувати
        .pipe(newer('app/images'))
        .pipe(webp())

        .pipe(src('app/images/src/*.*'))
        .pipe(newer('app/images'))
        .pipe(imagemin())

        .pipe(dest('app/images'))
}

// function for sprite svg convertation in css and html
function sprite () {
    return src('app/images/*.svg')
        .pipe(svgSprite({
            mode: {
                stack: {
                    sprite: '../sprite.svg',
                    example: true
                }
            }
        }))
        .pipe(dest('app/images'))
}

function pages() {
    return src('app/pages/*.html')
        .pipe(include({
            includePaths: 'app/components'
        }))
        .pipe(dest('app'))
        .pipe(browserSync.stream());
}
// function for convertation fonts in woff and ttf format
function fonts() {
    return src('app/fonts/src/*.*')
        .pipe(fonter({
            formats: ['woff', 'ttf']
        }))
        .pipe(src('app/fonts/*.ttf'))
        .pipe(ttf2woff2())
        .pipe(dest('app/fonts/src'));
}
// function для конкретинації файлів  з js до min.js
function scripts() {
    return src('app/js/main.js')
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .pipe(dest('app/js'))
        .pipe(browserSync.stream());
}
// function  автоматизації конкатенації
function watching() {
    browserSync.init({
        server: {
            baseDir: "app/"
        }
    });
    watch(['app/scss/style.scss'], styles)
    watch(['app/images/src'], images)
    watch(['app/js/main.js'], scripts)
    watch(['app/components/*', 'app/pages/*'], pages)
    watch(['app/*.html']).on('change', browserSync.reload);
}
// function for building project
function building() {
    return src([
        'app/components/*.html',
        'app/pages/*.html',
        'app/*.html',
        'app/css/style.min.css',
        '!src/images/stack/*.html',
        'app/images/*.*',
        '!app/images/*.svg',
        'app/images/sprite.svg',
        'app/fonts/*.*',
        'app/js/main.min.js'
    ], {base: 'app'})
        .pipe(dest('dist'));
}
// function for clean past build
function cleanDist() {
    return src('dist')
        .pipe(clean());
}


exports.styles = styles;
exports.images = images;
exports.fonts = fonts;
exports.pages = pages;
exports.scripts = scripts;
exports.building = building;
exports.sprite = sprite;
exports.watching = watching;

exports.build = series(cleanDist, building);
exports.default  = parallel(styles, images, scripts, pages, watching)

